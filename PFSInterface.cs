﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace PFS_System
{

    public class PFSInterface
    {

        const string PFSDllName = "socket_com.dll";
        private string ipAddress = string.Empty;
        private int bufSize = 1024;
        private string database = string.Empty;
        private string user = string.Empty;
        public string User { get { return user; } }
        private string password = string.Empty;
        private string assemblyNum;
        private string serialNumber;
        private string operationCode;

        [DllImport(PFSDllName, EntryPoint = "pfs_login")]
        private static extern int lclPFSLogin(string request, StringBuilder response, int bufSize, string address);

        [DllImport(PFSDllName, EntryPoint = "pfs_query")]
        private static extern int lclPFSQuery(string request, StringBuilder response, int bufSize, string address);

        [DllImport(PFSDllName, EntryPoint = "pfs_send_result")]
        private static extern int lclPFSSendResults(string request, StringBuilder response, int bufSize, string address);
        public PFSInterface(string serverIPAddress, string databaseName, string opCode)
        {
            this.database = databaseName;
            this.ipAddress = serverIPAddress;
            this.operationCode = opCode;

        }

        public bool GetUserInfo(ref string response)
        {
            GetSigninInfo signin = new GetSigninInfo();
            signin.ShowDialog();
            user = signin.User;
            password = signin.Password;

            return true;
        }

        public bool IsPFSInitialized { get { return (ipAddress != string.Empty); } }

        /// <summary>
        /// Log into PFS. Will return true if successful.
        /// </summary>
        /// <param name="database">database name to connect to</param>
        /// <param name="user">user for database</param>
        /// <param name="password">password for database</param>
        /// <param name="address">database IP address</param>
        /// <param name="response">Possible responses:
        ///                         OK[CR+LF{requested data}]
        ///                         {procedure name} Warning: {warning message}
        ///                         {procedure name} Failure: {failure message}
        ///                         {procedure name} Error: {error message}
        ///                         DLL Error: {error message}
        /// <returns>true = successful</returns>
        public bool PSFLogin(ref string response)
        {
            string request = string.Empty;
            StringBuilder rntStr = new StringBuilder(bufSize);
            int rntvalue = 0;

            if ((user == string.Empty) | (password == string.Empty))
            {
                response = "User and Password can not be blank.";
                return false;
            }
            // construct the request
            request = string.Format("DATABASE={0}\nUSER_ID={1}\nPASSWORD={2}\n\n", database, user, password);
            try
            {
                rntvalue = lclPFSLogin(request, rntStr, bufSize, ipAddress);
                response = rntStr.ToString();
            }
            catch (Exception ex)
            {
                response = string.Format("DLL Error: {0}", ex.Message);
                return false;
            }

            return response.ToUpper().Contains("OK");
        }

        /// <summary>
        /// Does a query to the PFS. 
        /// </summary>
        /// <param name="assemblyNum"></param>
        /// <param name="serialNumber"></param>
        /// <param name="operationCode"></param>
        /// <returns></returns>
        public bool PSFQuery(string assemblyNum, string serialNumber, string operationCode, ref string response)
        {
            string request = string.Empty;
            StringBuilder rntStr = new StringBuilder(bufSize);
            int rntvalue = 0;
            
            // Verify that the database stuff is set up
            if ((database == string.Empty) | (user == string.Empty) | (password == string.Empty))
            {
                response = string.Format("PSF Error: PFSLogin has not been called");
                return false;
            }

            this.assemblyNum = assemblyNum;
            this.serialNumber = serialNumber;
            this.operationCode = operationCode;

            // construct the request. Use the database info from when PSFLogin was called
            request = string.Format("DATABASE={0}\nUSER_ID={1}\nPASSWORD={2}\nASSEMBLY_NUM={3}\nSERIAL_NUM={4}\nOPERATION_CODE={5}\n\n",
                database, user, password, assemblyNum, serialNumber, operationCode);
            try
            {
                rntvalue = lclPFSQuery(request, rntStr, bufSize, ipAddress);
                response = rntStr.ToString();
            }
            catch (Exception ex)
            {
                response = string.Format("DLL Error: {0}", ex.Message);
                return false;
            }


            return response.ToUpper().Contains("OK");
        }

        /// <summary>
        /// Will send the results to PFS. Assumes that PSFQuery was already run for the part. This will use
        /// data from PFSQuery and PSFLogin.
        /// </summary>
        /// <param name="workCenter"></param>
        /// <param name="passFail"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public bool PSFSendResults(string workCenter, bool passFail, ref string response)
        {
            string request = string.Empty;
            StringBuilder rntStr = new StringBuilder(bufSize);
            int rntvalue = 0;
            string passFailstr;

            // Verify that the database stuff is set up
            if ((assemblyNum == string.Empty) | (serialNumber == string.Empty) | (operationCode == string.Empty))
            {
                response = string.Format("PSF Error: PFSQuery has not been called");
                return false;
            }

            passFailstr = passFail ? "P" : "F";

            // construct the request. Use the database info from when PSFLogin and PSFQuery was called
            request = string.Format("DATABASE={0}\nUSER_ID={1}\nPASSWORD={2}\nASSEMBLY_NUM={3}\nSERIAL_NUM={4}\nOPERATION_CODE={5}\nWORK_CENTER={6}\nPASS_FAIL={7}\n\n",
                database, user, password, assemblyNum, serialNumber, operationCode, workCenter, passFailstr);
            try
            {
                rntvalue = lclPFSSendResults(request, rntStr, bufSize, ipAddress);
                response = rntStr.ToString();
            }
            catch (Exception ex)
            {
                response = string.Format("DLL Error: {0}", ex.Message);
                return false;
            }


            return response.ToUpper().Contains("OK");
        }

        /// <summary>
        /// Will send the results to PFS. Assumes that PSFQuery was already run for the part. This will use
        /// data from PFSQuery and PSFLogin.
        /// </summary>
        /// <param name="workCenter"></param>
        /// <param name="passFail"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public bool PSFSendFailResults(string workCenter, string testName, string comment, ref string response)
        {
            string request = string.Empty;
            StringBuilder rntStr = new StringBuilder(bufSize);
            int rntvalue = 0;

            // Verify that the database stuff is set up
            if ((assemblyNum == string.Empty) | (serialNumber == string.Empty) | (operationCode == string.Empty))
            {
                response = string.Format("PSF Error: PFSQuery has not been called");
                return false;
            }


            // construct the request. Use the database info from when PSFLogin and PSFQuery was called
            request = string.Format("DATABASE={0}\nUSER_ID={1}\nPASSWORD={2}\nASSEMBLY_NUM={3}\nSERIAL_NUM={4}\nOPERATION_CODE={5}\nWORK_CENTER={6}\nPASS_FAIL=F\n",
                database, user, password, assemblyNum, serialNumber, operationCode, workCenter);
            request = string.Format("{0}FAILURE_CODE={1}\nFAILURE_COMMENT={2}\n\n", request, testName, comment);

            try
            {
                rntvalue = lclPFSSendResults(request, rntStr, bufSize, ipAddress);
                response = rntStr.ToString();
            }
            catch (Exception ex)
            {
                response = string.Format("DLL Error: {0}", ex.Message);
                return false;
            }


            return response.ToUpper().Contains("OK");
        }

    
    }
}
